import java.sql.*;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Main {

	private static Random r = new Random();
	static Scanner sc = new Scanner(System.in);;
    private static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException ignored) {
        }
    }
	
	public static void main(String[] args) throws SQLException, ClassNotFoundException {
		DBmanagerTemperatur dbm = DBmanagerTemperatur.getInstance();
		
		
		do {
			System.out.println("Befehle:");
			System.out.println("i . . . Temperatur jetzt");
			System.out.println("z . . . Temperatur bestimmter Tag");
			System.out.println("r . . . Gibt die Temperaturen als Graph aus");
			String choice = sc.next();
			switch(choice) {
			case "i":  
				System.out.println("Einzutragende Temp?");
				dbm.addTemp(sc.nextDouble());
				break;
			case "z":
				System.out.println("Jahr?");
				int year = sc.nextInt();
				System.out.println("Monat?");
				int month = sc.nextInt();
				System.out.println("Tag?");
				int day = sc.nextInt();
				@SuppressWarnings("deprecation") Date date = new Date(year, month, day);
				System.out.println("Einzutragende Temp?");
				dbm.addTempDate(sc.nextDouble(), new java.sql.Timestamp(date.getTime()));
				break;
			case "r": dbm.printData();
				break;
			}
		}while(true);
		
	}

}
