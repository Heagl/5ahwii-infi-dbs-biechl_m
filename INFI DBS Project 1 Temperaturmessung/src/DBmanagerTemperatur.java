import java.awt.Frame;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import org.knowm.xchart.*;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;


public class DBmanagerTemperatur {

	private static DBmanagerTemperatur _instance;

	//Singleton
	private DBmanagerTemperatur() throws ClassNotFoundException {
		Class.forName("com.mysql.jdbc.Driver");
	}
	public static DBmanagerTemperatur getInstance() throws ClassNotFoundException {
		if(_instance == null) {
			_instance = new DBmanagerTemperatur();
		}
		return _instance;
	}

	//private Functions
	public Connection getConnection() throws SQLException {
		String dbname = "temperaturmessung";
		String user = "schule";
		String pwd = "schule";
		String conStr = "jdbc:mysql://localhost:3306/temperaturmessung?useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=Europe/Berlin";
		return (Connection) DriverManager.getConnection(conStr,user,pwd);
	}

	public void releaseConnection(Connection con) throws SQLException {
		con.close();
	}

	public void addTemp(double temp) throws SQLException {
		java.sql.PreparedStatement stm = null;
		Connection con = null;
		Calendar c = Calendar.getInstance();

		try {
			con = getConnection();

			String query = "INSERT INTO temperaturmesswerte" +
					"(temperatur, time) "+ 
					"VALUES (?,?)";
			stm = con.prepareStatement(query);
			stm.setDouble(1, temp);
			stm.setTimestamp(2, new Timestamp(c.getTimeInMillis()));
			stm.executeUpdate();
		}
		finally {

			if(stm != null) {
				stm.close();
			}

			if(con != null) {
				this.releaseConnection(con);
			}

		}
	}
	public void addTempDate(double temp, Timestamp date) throws SQLException {
		java.sql.PreparedStatement stm = null;
		Connection con = null;
		ResultSet rs = null;

		try {
			con = getConnection();

			String query = "INSERT INTO temperaturmessung" +
					"(temperatur, time) "+ 
					"VALUES (?,?)";
			stm = con.prepareStatement(query);
			stm.setDouble(1, temp);
			stm.setTimestamp(2, date);
		}
		finally {

			if(stm != null) {
				stm.close();
			}

			if(con != null) {
				this.releaseConnection(con);
			}

		}
	}

	public void printData() throws SQLException {
		Connection con = getConnection();
		java.sql.Statement stmt = con.createStatement();
		ResultSet rs = stmt.executeQuery("select * from temperaturmesswerte Order By Time");
		int i = 1;
		List<Double> resultsTemp = new ArrayList<Double>();
		List<Integer> resultsDate = new ArrayList<Integer>();
		try {
			con = getConnection();
			while(rs.next())
			{
				resultsTemp.add(rs.getDouble("Temperatur"));
				resultsDate.add(i);
				i++;
			}

			XYChart chart = QuickChart.getChart("Temperatur Chart", "X", "Y", "y(x)", resultsDate, resultsTemp);
			//System.out.println(chart);
			new SwingWrapper(chart).displayChart();
		}
		finally {
			if(rs != null) {
				rs.close();
			}

			i = 0;

			if(con != null) {
				this.releaseConnection(con);
			}
		}
	}
}
