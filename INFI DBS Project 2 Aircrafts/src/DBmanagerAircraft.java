import java.awt.Frame;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import org.knowm.xchart.*;
import org.knowm.xchart.internal.chartpart.Chart;
import org.knowm.xchart.style.Styler.LegendPosition;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;



public class DBmanagerAircraft {


	private static DBmanagerAircraft _instance;

	//Singleton
	private DBmanagerAircraft() throws ClassNotFoundException {
		Class.forName("com.mysql.jdbc.Driver");
	}

	public static DBmanagerAircraft getInstance() throws ClassNotFoundException {
		if(_instance == null) {
			_instance = new DBmanagerAircraft();
		}
		return _instance;
	}

	public Connection getConnection() throws SQLException {
		String user = "school";
		String pwd = "school";
		String conStr = "jdbc:mysql://localhost:3306/aircraft";
		return (Connection) DriverManager.getConnection(conStr,user,pwd);
	}

	public void releaseConnection(Connection con) throws SQLException {
		con.close();
	}

	public CategoryChart getChart(List<String> resultsMonth, List<Integer> results) {

		// Create Chart
		CategoryChart chart = new CategoryChartBuilder().width(1500).height(600).title("Score Histogram").xAxisTitle("Month").yAxisTitle("Flights").build();
		// Customize Chart
		chart.getStyler().setLegendPosition(LegendPosition.InsideNW);
		chart.getStyler().setHasAnnotations(true);
		// Series

		chart.addSeries("Flights",resultsMonth , results);

		return chart;
	}

	public void printData() throws SQLException {
		Connection con = getConnection();
		ResultSet rs = null;
		java.sql.Statement stmt = con.createStatement();
		List<Integer> results = new ArrayList<>();
		List<String> resultsMonth = new ArrayList<String>();
		try {
			con = getConnection();
			rs = stmt.executeQuery("select  MONTH(FROM_UNIXTIME(seentime)), YEAR(FROM_UNIXTIME(seentime)), COUNT(distinct flightnr)"+
					" FROM dump1090data"+
					" GROUP BY MONTH(FROM_UNIXTIME(seentime)), YEAR(FROM_UNIXTIME(seentime)) order by YEAR(FROM_UNIXTIME(seentime)), month(from_unixtime(seentime))");
			while(rs.next())
			{
				results.add(rs.getInt(3));
				int month = rs.getInt(1);
				int year = rs.getInt(2);

				switch (month) {
				case 1:
					resultsMonth.add("January "+ year);
					break;

				case 2:
					resultsMonth.add("Feburary "+ year);
					break;
				case 3:
					resultsMonth.add("March "+ year);
					break;

				case 4:
					resultsMonth.add("April "+ year);
					break;

				case 5:
					resultsMonth.add("May "+ year);
					break;

				case 6:
					resultsMonth.add("June "+ year);
					break;

				case 7:
					resultsMonth.add("July "+ year);
					break;

				case 8:
					resultsMonth.add("August "+ year);
					break;

				case 9:
					resultsMonth.add("September "+ year);
					break;

				case 10:
					resultsMonth.add("October "+ year);
					break;

				case 11:
					resultsMonth.add("November "+ year);
					break;

				case 12:
					resultsMonth.add("December "+ year);
					break;
				}

//				if(month == 1) resultsMonth.add("January "+ year);
//				if(month == 2) resultsMonth.add("February "+ year);
//				if(month == 3) resultsMonth.add("March "+ year);
//				if(month == 4) resultsMonth.add("April "+ year);
//				if(month == 5) resultsMonth.add("May "+ year);
//				if(month == 6) resultsMonth.add("June "+ year);
//				if(month == 7) resultsMonth.add("July "+ year);
//				if(month == 8) resultsMonth.add("August "+ year);
//				if(month == 9) resultsMonth.add("September "+ year);
//				if(month == 10) resultsMonth.add("October "+ year);
//				if(month == 11) resultsMonth.add("November "+ year);
//				if(month == 12) resultsMonth.add("December "+ year);
			}
			CategoryChart chart = getChart(resultsMonth, results);
			new SwingWrapper<CategoryChart>(chart).displayChart();

		}
		finally {
			if(rs != null) {
				rs.close();
			}

			if(con != null) {
				this.releaseConnection(con);
			}
		}
	}
}
